package com.zycus.zyrichservice.management;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.zycus.zyrich.atoms.DBConnection;
import com.zycus.zyrich.atoms.IndexConfig;
import com.zycus.zyrich.atoms.JobConfig;
import com.zycus.zyrich.atoms.User;
import com.zycus.zyrich.core.SupplierPKConfidenceDTO;
import com.zycus.zyrich.dao.StatusTableDAO;
import com.zycus.zyrich.exceptions.ZyrichException;
import com.zycus.zyrich.taskmanagement.SearchTask;
import com.zycus.zyrich.utils.ConnectionPoolHandler;
import com.zycus.zyrichservice.model.ZyrichRequest;

public class SearchTaskJBPM extends SearchTask{

	private Logger logger = Logger.getLogger(SearchTaskJBPM.class);

	public SearchTaskJBPM(IndexConfig indexConfig, JobConfig jobConfig, User user) throws ZyrichException {
		super(indexConfig, jobConfig, user);
	}
	
	public Map<String,String> runNewForSupplier(ZyrichRequest zyRequest) throws Exception{

	
		statusTableDAO = new StatusTableDAO();
		List<String> rawColumnList = new ArrayList<String>();

		rawColumnList.addAll(this.jobConfig.getFieldsToPopulate());

		List<String> processedRawList = new ArrayList<String>(rawColumnList);
		processedRawList = processfieldsToPopulate(rawColumnList);

		
		setFieldsToPopulate(rawColumnList);
		this.efxColumnList = processedRawList;

		try {
			
			SupplierPKConfidenceDTO key=searchForSuppRisk(zyRequest.getReqColumnName());
			if(StringUtils.isNotBlank(key.getPrimaryKey())) {
				return fireFinalUpdateForSupplierData(processedRawList, rawColumnList,key);
				
			}

		} finally {
		}

		
		return null;

		
	}
	
	public boolean runNew(ZyrichRequest zyRequest) throws Exception{

		statusTableDAO = new StatusTableDAO();
		List<String> rawColumnList = new ArrayList<String>();

		rawColumnList.addAll(this.jobConfig.getFieldsToPopulate());

		List<String> processedRawList = new ArrayList<String>(rawColumnList);
		processedRawList = processfieldsToPopulate(rawColumnList);

		List<String> listToCreateColumns = this.createColumnList(rawColumnList);
		
		setFieldsToPopulate(rawColumnList);
		this.efxColumnList = processedRawList;

		String jobName = this.jobConfig.getJobName();
		String userName = this.user.getLoginname();
		boolean DATALINK_OPENED_BY_THIS_APPLICATION = false;
		try {
			if (openDataLink()) {
				try {
					DATALINK_OPENED_BY_THIS_APPLICATION = true;
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Removing Vestigeal Columns.");
					removeColumns(listToCreateColumns);
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Removed Columns Successfully.");
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Creating Columns.");
					createColumns(listToCreateColumns);
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Columns Successfully Created.");
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Searching");
					
					search();

					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Search Complete");
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Final Updation Ongoing");
					fireFinalUpdate(processedRawList, rawColumnList);
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Generating Confidence Predictions");
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Job Terminated Successfully.");
					new StatusTableDAO().updateState(zyRequest.getDisplayName(), userName, "PASSIVE");

				} catch (ZyrichException e) {
					try {
						statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, e.toString());
						new StatusTableDAO().updateState(zyRequest.getDisplayName(), userName, "PASSIVE");
					} catch (ZyrichException e1) {
						logger.error(e);
						throw e;
					}
					logger.error("ZyrichException", e);
					throw e;
				} catch (Exception e) {
					try {
						statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, e.toString());
						new StatusTableDAO().updateState(zyRequest.getDisplayName(), userName, "PASSIVE");
					} catch (ZyrichException e1) {
						logger.error(e1);
						throw e;
					}
					logger.error("Unexpected Exception", e);
					throw e;
				}
			} else {
				try {
					statusTableDAO.updateStatus(zyRequest.getDisplayName(), userName, "Unable To Connect To UserDB");
					new StatusTableDAO().updateState(zyRequest.getDisplayName(), userName, "PASSIVE");

				} catch (Exception e) {
					logger.error("Unexpected Exception", e);
					throw e;
				}
			}

		} finally {
			cleanUp(DATALINK_OPENED_BY_THIS_APPLICATION);
		}

		
		return true;

		
	}
	@Override
	protected boolean openDataLink() {
		Connection connection = null;
//		boolean result=false;
		try {
			DBConnection dbConnection = jobConfig.getUserDbConnDetails();
			connection = new ConnectionPoolHandler()
			.getConnectionfromPool("APPLICATION");

			String linkName=this.user.getLoginname()+ "_" + this.indexConfig.getName();
			String checkQuery="SELECT db_link FROM ALL_DB_LINKS WHERE db_link =?";
			String dropQuery="DROP DATABASE LINK "+linkName;

			PreparedStatement checkStatement=connection.prepareStatement(checkQuery);
			checkStatement.setString(1, linkName);
			
			checkStatement.execute();
			
			ResultSet checkSet = checkStatement.getResultSet();
			
			if(checkSet.next()){
								
				Statement dropStatement = connection.createStatement();
				
				try {
					logger.info("DROPPING LINK QUERY: "+dropQuery);
					if(dropStatement.execute(dropQuery)){
//						result=true;
					}
				} catch (Exception e) {
					e.printStackTrace();
					throw e;
				}
				
			}
						
			
			StringBuffer buffer = new StringBuffer();
			buffer.append("CREATE DATABASE LINK " + linkName + " connect to "
					+ dbConnection.getUserName() + " identified by "
					+ dbConnection.getPassword() + " using '"
					+ dbConnection.getSid() + "'");
			logger.info(buffer.toString());

			PreparedStatement statement = connection.prepareStatement(buffer
					.toString());
			statement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;

		} finally {
			try {
				connection.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
