package com.supplierrisk.supplierrisk.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author joydeep.chakraborty
 *
 */
public class SupplierSearchOutputDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String accountName;
	private LocationDTO location;
	private Map<String,String> requiredInfo;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public LocationDTO getLocation() {
		return location;
	}
	public void setLocation(LocationDTO location) {
		this.location = location;
	}
	public Map<String, String> getRequiredInfo() {
		return requiredInfo;
	}
	public void setRequiredInfo(Map<String, String> requiredInfo) {
		this.requiredInfo = requiredInfo;
	}
	
	
}

