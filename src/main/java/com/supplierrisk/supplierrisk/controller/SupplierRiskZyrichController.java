package com.supplierrisk.supplierrisk.controller;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.supplierrisk.supplierrisk.dto.BatchSupplierOutputDTO;
import com.supplierrisk.supplierrisk.dto.BatchTenantSearchDTO;
import com.supplierrisk.supplierrisk.dto.ErrorDTO;
import com.supplierrisk.supplierrisk.dto.LocationDTO;
import com.supplierrisk.supplierrisk.dto.SupplierSearchOutputDTO;
import com.supplierrisk.supplierrisk.dto.TenantSearchDTO;
import com.supplierrisk.supplierrisk.task.SupplierSearchTask;
import com.zycus.zyrich.utils.ISOAlphaNumericGenerator;
import com.zycus.zyrich.utils.SpecialCharacterCleansing;
import com.zycus.zyrichservice.invoked.InvokeCleaning;
import com.zycus.zyrichservice.invoked.ZyrichInvoked;

@RestController
@RequestMapping("/supplierZyrichIntegration")
public class SupplierRiskZyrichController {
	
	private static final String ACCOUNT_NAME = "accountName";
	private static final String SUPPLIER_COUNTRY = "country";
	private static final String SUPPLIER_CITY = "city";
	private static final String SUPPLIER_STATE = "state";
	private static final String SUPPLIER_ZIPCODE = "zipCode";
	private static ISOAlphaNumericGenerator isoAlphaNumericGenerator;

	@Value("${LEUCENE_REPO_NAME}")
	private String LEUCENE_REPO_NAME ;
	
	private static final Log logger = LogFactory.getLog(SupplierRiskZyrichController.class);
	
	ExecutorService supplierSearchThreadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	
	@POST
	@RequestMapping(path="/search")
	public Object getSuppliersForTenant(@RequestBody TenantSearchDTO tenantSearchDTO) {
		try {
			SupplierSearchOutputDTO supplierSearchOutputDTO = new SupplierSearchOutputDTO();
			Map<String, String> context_param = processInput(tenantSearchDTO);

			ZyrichInvoked zy = new ZyrichInvoked();
			Map<String, String> mapOFValues = zy.runJobInTOS(context_param);
			
			if (mapOFValues.get("DUNS_NUM") != null) {

				mapOFValues.put("DUNS_NUM",
						Base64.getEncoder().encodeToString(mapOFValues.get("DUNS_NUM").getBytes("utf-8")));

			}
			supplierSearchOutputDTO.setRequiredInfo(mapOFValues);
			supplierSearchOutputDTO.setAccountName(tenantSearchDTO.getSearchParams().get(ACCOUNT_NAME));
			supplierSearchOutputDTO.setLocation(getLocationDTO(tenantSearchDTO));
			return supplierSearchOutputDTO;
		} catch (Exception e) {
			ErrorDTO error = new ErrorDTO();
			error.setMessage("Error occured " + e);
			return Response.ok().entity(error).build();
		}

	}
	
	private LocationDTO getLocationDTO(TenantSearchDTO tenantSearchDTO) {
		LocationDTO location=new LocationDTO();
		location.setCountry(tenantSearchDTO.getSearchParams().get(SUPPLIER_COUNTRY));
		location.setCity(tenantSearchDTO.getSearchParams().get(SUPPLIER_CITY));
		location.setState(tenantSearchDTO.getSearchParams().get(SUPPLIER_STATE));
		location.setZipCode(tenantSearchDTO.getSearchParams().get(SUPPLIER_ZIPCODE));
		return location;
	}

	@POST
	@RequestMapping(path="/batchSearchold")
	public Object getAllSupplierDetails(@RequestBody BatchTenantSearchDTO tenantSearchDTO) {
		BatchSupplierOutputDTO output = new BatchSupplierOutputDTO();
		output.setBatchId(tenantSearchDTO.getBatchId());
		Map<String, SupplierSearchOutputDTO> mapOFSupplierTODunsData = new HashMap<String, SupplierSearchOutputDTO>();

		try {
			ZyrichInvoked zy = new ZyrichInvoked();

			for (Entry<String, TenantSearchDTO> suppLierMap : tenantSearchDTO.getMapOFSupplierTODunsData().entrySet()) {
				SupplierSearchOutputDTO supplierSearchOutputDTO = new SupplierSearchOutputDTO();
				Map<String, String> mapOFValues = null;
				try {

					Map<String, String> context_param = processInput(suppLierMap.getValue());

					mapOFValues = zy.runJobInTOS(context_param);
					if (null != mapOFValues && !mapOFValues.isEmpty()) {

						if (mapOFValues.get("DUNS_NUM") != null) {

							mapOFValues.put("DUNS_NUM",
									Base64.getEncoder().encodeToString(mapOFValues.get("DUNS_NUM").getBytes("utf-8")));

						}
						supplierSearchOutputDTO.setRequiredInfo(mapOFValues);
						supplierSearchOutputDTO
								.setAccountName(suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME));
						supplierSearchOutputDTO.setLocation(getLocationDTO(suppLierMap.getValue()));
						mapOFSupplierTODunsData.put(
								suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
										+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY),
								supplierSearchOutputDTO);
					} else {
						mapOFSupplierTODunsData.put(suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
								+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY), null);
						logger.info("Error NO data found for  : "
								+ suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
								+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY));
					}
				} catch (Exception e) {
					mapOFSupplierTODunsData.put(suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
							+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY), null);
					logger.info("Error NO data found with error for  : "
							+ suppLierMap.getValue().getSearchParams().get(ACCOUNT_NAME) + "|"
							+ suppLierMap.getValue().getSearchParams().get(SUPPLIER_COUNTRY));
					continue;
				}

			}
			output.setMapOFSupplierTODunsData(mapOFSupplierTODunsData);
			return output;
		} catch (Exception e) {
			return Response.ok().entity(output).build();
		}

	}
	
	
	@POST
	@RequestMapping(path="/batchSearch")
	public Object getAllSupplierDetailsnew(@RequestBody BatchTenantSearchDTO tenantSearchDTO) {
		BatchSupplierOutputDTO output = new BatchSupplierOutputDTO();
		output.setBatchId(tenantSearchDTO.getBatchId());
		Map<String, SupplierSearchOutputDTO> mapOFSupplierTODunsData = new HashMap<String, SupplierSearchOutputDTO>();

		try {
			ZyrichInvoked zy = new ZyrichInvoked();
			CountDownLatch latch = new CountDownLatch(tenantSearchDTO.getMapOFSupplierTODunsData().size());
			for (Entry<String, TenantSearchDTO> suppLierMap : tenantSearchDTO.getMapOFSupplierTODunsData().entrySet()) {
				SupplierSearchTask task=new SupplierSearchTask(mapOFSupplierTODunsData, zy,suppLierMap,LEUCENE_REPO_NAME,latch);
				supplierSearchThreadPool.execute(task);
			}
			latch.await();
			output.setMapOFSupplierTODunsData(mapOFSupplierTODunsData);
			return output;
		} catch (Exception e) {
			return Response.ok().entity(output).build();
		}

	}

	
	@GET
	@RequestMapping(path="/test")
	public String test() {
	
		return "SUCCESS";
	}

	private Map<String, String> processInput(TenantSearchDTO tenantSearchDTO) throws IOException {
		Map<String, String> context_param=new HashMap<String, String>();
		isoAlphaNumericGenerator=new ISOAlphaNumericGenerator();

	
		context_param.put("ZY_FIELDS", StringUtils.join(tenantSearchDTO.getRequiredInfo(), ","));
		context_param.put("ZY_REPOSITORY_NAME", LEUCENE_REPO_NAME);  //put pro file
		context_param.put("ZY_TAGS", ""); // may be reqd
		context_param.put("projectName", "Merlin");
		String suppName=null;
		String country=null;
		String state=null;
		String city=null;
		String zipCode=null;
		
		for(Entry<String, String> map:tenantSearchDTO.getSearchParams().entrySet()) {
			if(StringUtils.isNotBlank(map.getValue())) {
			if(map.getKey().equalsIgnoreCase("accountName")) {
				suppName=map.getValue();
				suppName=suppName.toLowerCase();				
				//CLEANING LOGIC
				//FOR SOME SUPPLIERS ENCLOSED IN DOUBLE QUOTES
//				suppName=suppName.replace("\"", "");
				suppName=InvokeCleaning.cleanVendorName(suppName);
			}else if(map.getKey().equalsIgnoreCase("country")) {
				country=map.getValue();
				country= isoAlphaNumericGenerator.getISOAlphaNumericCode(country);
				country=country.toLowerCase();
			}else if(map.getKey().equalsIgnoreCase("state")) {
				state=map.getValue();
				state = SpecialCharacterCleansing.replaceChar(state);

				if (country != null && country != "" && state != "" && state.length() > 0) {
					removeRegionCountry(state, country, null);
				}

				if (state == null || state == "") {
					state = "null";
				} else {
					state = isoAlphaNumericGenerator.getStateCode(state);
				}
				state=state.toLowerCase();
//				regionTemp = state;
			}else if(map.getKey().equalsIgnoreCase("city")) {
				city=map.getValue();
				city = SpecialCharacterCleansing.replaceChar(city);
				if (city != "") {
					removeRegionCountry(city, country, state);
				}

				if (city == null || city == ""  || city.trim().length() == 0) {
					city = "null";
				}
				city=city.toLowerCase();
			}else if(map.getKey().equalsIgnoreCase("zipCode")) {
				zipCode=map.getValue();
			}
		}
		}
		if(StringUtils.isBlank(suppName)) {
			throw new RuntimeException("Account name is mandatory");
		}
		context_param.put("ZY_COLUMN_MAPPINGS", "Vendor Name:"+suppName+",Country:"+country+",Region:"+state+",City:"+city+",Zipcode:"+zipCode);
		
	return context_param;
}

	protected static String removeRegionCountry(String arrValue, String countryTemp, String regionTemp) {
		if (arrValue.trim().length() != 0) {
			String[] arrvalueSplit = arrValue.split(" ");
			for (int arrayCounter = 0; arrayCounter < arrvalueSplit.length; arrayCounter++) {
				if (countryTemp != null && countryTemp != "" && countryTemp.equalsIgnoreCase(isoAlphaNumericGenerator.getISOAlphaNumericCodeChk(arrvalueSplit[arrayCounter]))) {
					arrValue = arrValue.replaceAll(arrvalueSplit[arrayCounter], "");
				}

				if (regionTemp != null && regionTemp != "" && regionTemp.equalsIgnoreCase(isoAlphaNumericGenerator.getStateCodeChk(arrvalueSplit[arrayCounter]))) {
					arrValue = arrValue.replaceAll(arrvalueSplit[arrayCounter], "");
				}
			}
		}

		return arrValue;
	}
}
