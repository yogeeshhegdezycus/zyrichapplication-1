package com.supplierrisk.supplierrisk.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * 
 * @author joydeep.chakraborty
 *
 */
public class TenantSearchDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Set<String> requiredInfo;
	private String repoName;
	private String tagName;
	private Map<String,String> searchParams;
	public Set<String> getRequiredInfo() {
		return requiredInfo;
	}
	public void setRequiredInfo(Set<String> requiredInfo) {
		this.requiredInfo = requiredInfo;
	}
	public String getRepoName() {
		return repoName;
	}
	public void setRepoName(String repoName) {
		this.repoName = repoName;
	}
	public String getTagName() {
		return tagName;
	}
	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	public Map<String, String> getSearchParams() {
		return searchParams;
	}
	public void setSearchParams(Map<String, String> searchParams) {
		this.searchParams = searchParams;
	}
	
	
	
	
	
}

